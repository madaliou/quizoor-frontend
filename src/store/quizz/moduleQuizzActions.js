/*=========================================================================================
  File Name: moduleTodoActions.js
  Description: Todo Module Actions
  ----------------------------------------------------------------------------------------
  Item Name: Vuexy - Vuejs, HTML & Laravel Admin Dashboard Template
  Author: Pixinvent
  Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/

//import axios from "@/axios.js"
import UserService from '@/services/user.service.js'

export default {
    setTodoSearchQuery({ commit }, query){
        commit('SET_TODO_SEARCH_QUERY', query)
    },
    
    fetchSubmitQuizzes({ commit }) {
      return UserService.getSubmitQuiz().then(
        response => {
          commit('SET_SUBMIT_QUIZZES', response.data)          
        },
        error => {
          return error;
        }
      )
    },    

    addSubmitQuiz({ commit }, payload) {
      //return new Promise((resolve, reject) => {

        return UserService.addSubmitQuiz(payload).then(
          response => {
            console.log('dataaaaaaaaa : ', response.data);
            commit('ADD_SUBMIT_QUIZZ', response.data)
            //resolve(response)
          },
          error => {
            return error;
          }
        )       
      //})
    },

    updateSubmitQuiz({ commit }, id, payload) {
      //return new Promise((resolve, reject) => {

        return UserService.updateSubmitQuiz(id, payload).then(
          response => {
            console.log('dataaaaaaaaa : ', response.data);
            commit('UPDATE_SUBMIT_QUIZZ', response.data);
            //resolve(response)
          },
          error => {
            return error;
          }
        )       
      //})
    },

    removeSubmitQuizz({ commit }, itemId) {
      return UserService.deleteSubmitQuiz(itemId).then(
        response => {
          console.log('delete : ');
          commit('DELETE_SUBMIT_QUIZZ', itemId);
          //resolve(response)
        },
        error => {
          return error;
        }
      )            
    },










    fetchRequestQuizzes({ commit }) {
      return UserService.getRequestQuiz().then(
        response => {
          commit('SET_REQUEST_QUIZZES', response.data)          
        },
        error => {
          return error;
        }
      )
    },    

    addRequestQuiz({ commit }, payload) {
      //return new Promise((resolve, reject) => {

        return UserService.addRequestQuiz(payload).then(
          response => {
            console.log('dataaaaaaaaa : ', response.data);
            commit('ADD_REQUEST_QUIZZ', response.data)
            //resolve(response)
          },
          error => {
            return error;
          }
        )       
      //})
    },

    updateRequestQuiz({ commit }, id, payload) {
      //return new Promise((resolve, reject) => {

        return UserService.updateRequestQuiz(id, payload).then(
          response => {
            console.log('dataaaaaaaaa : ', response.data);
            commit('UPDATE_REQUEST_QUIZZ', response.data.requestQuiz);
            //resolve(response)
          },
          error => {
            return error;
          }
        )       
      //})
    },

    removeRequestQuizz({ commit }, itemId) {
      return UserService.deleteRequestQuiz(itemId).then(
        response => {
          console.log('delete : ');
          commit('DELETE_REQUEST_QUIZZ', itemId);
          //resolve(response)
        },
        error => {
          return error;
        }
      )            
    },


   /*  addTask({ commit }) {
      return new Promise((resolve, reject) => {

        return UserService.addRequestQuiz().then(
          response => {
            commit('ADD_TASK', response.data)
            resolve(response)
          },
          error => {
            return error;
          }
        )
      })
    }, */

}
