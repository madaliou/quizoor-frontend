
export default [
    {
      url: "/user",
      name: "Dashboard",
      slug: "dashboard",
      icon: "HomeIcon",
      i18n: "Dashboard",
    },
    
    {
      header: "Pages",
      i18n: "Pages",
    },
    {
      url: "/user/requestedQuizz/all",
      name: "Requested Quizz",
      slug: "requestedQuizz",
      icon: "HelpCircleIcon",
      i18n: "requestedQuizz",
    },
    {
      url: "/user/submittedQuizz/all",
      name: "Submited Quizz",
      slug: "submittedQuizz",
      icon: "HelpCircleIcon",
      i18n: "submittedQuizz",
    },
    {
      url: null,
      name: "Quizz",
      icon: "HelpCircleIcon",
      i18n: "Quizz",
      submenu: [
        {
            url: "/user/quizz/all",
            name: "All",
            slug: "quizz-all",
            i18n: "All",
        },
        {
            url: "/user/quizz/activated",
            name: "Activated",
            slug: "quizz-activated",
            i18n: "Activated",
        },
        {
            url: "/user/quizz/deactivated",
            name: "Deactivated",
            slug: "quizz-deactivated",
            i18n: "Deactivated",
        },
        {
            url: "/user/quizz/unchecked",
            name: "Unchecked",
            slug: "quizz-unchecked",
            i18n: "Unchecked",
        },
      ]
    },
    
  ]
