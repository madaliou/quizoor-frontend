/*=========================================================================================
  File Name: moduleTodoMutations.js
  Description: Todo Module Mutations
  ----------------------------------------------------------------------------------------
  Item Name: Vuexy - Vuejs, HTML & Laravel Admin Dashboard Template
  Author: Pixinvent
  Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/


export default {
    SET_TODO_SEARCH_QUERY(state, query) {
        state.todoSearchQuery = query
    },
    UPDATE_TODO_FILTER(state, filter) {
      state.todoFilter = filter
    },

    // API
    SET_TASKS(state, tasks) {
      //state.tasks = tasks
      state.requestQuizzes = tasks
    },   

    SET_TAGS(state, tags) {
      state.taskTags = tags
    },
    ADD_TASK(state, task) {
      state.requestQuizzes.unshift(task)
    },

    UPDATE_TASK(state, task) {
      const taskIndex = state.tasks.findIndex((t) => t.id == task.id)
      Object.assign(state.tasks[taskIndex], task)
    },



    SET_SUBMIT_QUIZZES(state, tasks) {
      state.submitQuizzes = tasks
    },

    ADD_SUBMIT_QUIZZ(state, task) {
      state.submitQuizzes.unshift(task)
    },   

    UPDATE_SUBMIT_QUIZZ(state, task) {
      const taskIndex = state.submitQuizzes.findIndex((t) => t._id == task._id)
      Object.assign(state.submitQuizzes[taskIndex], task)
    },    

    DELETE_SUBMIT_QUIZZ(state, itemId) {
      const ItemIndex = state.submitQuizzes.findIndex((p) => p.id == itemId)
      state.submitQuizzes.splice(ItemIndex, 1)
  },


  SET_REQUEST_QUIZZES(state, tasks) {
    state.requestQuizzes = tasks
  },
  
  ADD_REQUEST_QUIZZ(state, task) {
    state.requestQuizzes.unshift(task)
  },   

  UPDATE_REQUEST_QUIZZ(state, task) {
    const taskIndex = state.requestQuizzes.findIndex((t) => t._id == task._id)
    Object.assign(state.requestQuizzes[taskIndex], task)
  },    

  DELETE_REQUEST_QUIZZ(state, itemId) {
    const ItemIndex = state.requestQuizzes.findIndex((p) => p.id == itemId)
    state.requestQuizzes.splice(ItemIndex, 1)
},
    
    
}
