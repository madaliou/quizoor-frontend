import ClientService from '../services/client.service';

import {
  GETSUBJECTITEMS,
  GETLEVLEITEMS,
  GETTRACKITEMS,
  UPDATEQUIZITEM,
  GETESTITEM,
  GETALLSUBJECT,
  SEARCHSUBJECT,
  ISLOADING
} from './actionType';

import {
  GETSUBJECTITEMSSUCCESS,
  GETLEVELITEMSSUCCESS,
  GETTRACKITEMSSUCCESS,
  UPDATEQUIZITEMSUCCESS,
  GETALLSUBJECTSUCCESS,
  SEARCHSUBJECTSUCCESS,LOADING
  // GETESTITEMSUCCESS
} from './mutationType';
const user  = JSON.parse(localStorage.getItem('user'));

const state = {
  refresh: false,
  user: user,
  levelItems: [],
  categories: [],
  chapterItems: [],
  subjectItems: [],
  localeSubjectItems: [],
  quizItems: [],
  subjectInformation: [],
  chapterSummary: [],
  chapterName: '',
  loading : false,
  // getTestItems: Object
}

const actions = {
  async [GETSUBJECTITEMS](context, locale) {
    const res = await ClientService.getSubjects(locale);
    context.commit(GETSUBJECTITEMSSUCCESS, res);
  },
  async [GETLEVLEITEMS](context, locale) {
    const res = await ClientService.getSearchValue(locale);
    context.commit(GETLEVELITEMSSUCCESS, res);
  },
  async [GETTRACKITEMS](context, rdata) {
    const res = await ClientService.getSearchTrakItems(rdata);
    context.commit(GETTRACKITEMSSUCCESS, res);
  },
  async [UPDATEQUIZITEM](context, rdata) {
    const res = await ClientService.updateQuizItem(rdata);
    context.commit(UPDATEQUIZITEMSUCCESS, res);
  },
  async [GETESTITEM](context, rdata) {
    const res = await ClientService.getTestItem(rdata);
    return res;
  },
  async [GETALLSUBJECT](context, rdata) {
    const res = await ClientService.getAllSubjectItems(rdata);
    context.commit(GETALLSUBJECTSUCCESS, res);
  },
  async [SEARCHSUBJECT](context, rdata) {
    const res = await ClientService.searchSubject(rdata);
    context.commit(SEARCHSUBJECTSUCCESS, res);
    return res;
  },
  async [ISLOADING](context) {
    context.commit(LOADING);
  }
};

const mutations = {
  [GETSUBJECTITEMSSUCCESS](state, res) {
    state.subjectItems = res.data;
    state.loading = false;
  },
  [GETLEVELITEMSSUCCESS](state, res) {
    state.levelItems = res.data;
    state.loading = false;
  },
  [GETTRACKITEMSSUCCESS](state,res) {
    state.subjectInformation = res.data.subjectInformation
    state.chapterItems = res.data.chapterItems;
    state.loading = false;
    if(res.data.quizItems.length == 0) {
      state.quizItems = '';
      state.chapterSummary = '';
      state.chapterName = '';
    } else if(res.data.quizItems.length > 0) {
      state.quizItems = res.data.quizItems;
      state.chapterSummary = res.data.quizItems[0].chapterContent;
      state.chapterName = res.data.quizItems[0].chapterName;
    }
  },
  [UPDATEQUIZITEMSUCCESS](state, res) {
    state.quizItems = res.data.quizItems;
    state.chapterSummary = res.data.chapterItems.content;
    state.chapterName = res.data.chapterItems.name;
    state.loading = false;
  },
  [GETALLSUBJECTSUCCESS](state, res) {
    state.localeSubjectItems = res.data;
    state.loading = false;
  },
  [SEARCHSUBJECTSUCCESS](state, res) {
    state.subjectItems = res.data;
    state.loading = false;
  },
  [LOADING](state) {
    state.loading = true;
  },
}

export default {
  state,
  actions,
  mutations
}
