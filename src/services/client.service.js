import axios from "axios";
import { API_URL } from "../services/config";
import authHeader from "./auth.header";

class ClientService {
  async getSearchValue(rdata) {
    try {
      let location = rdata;

      const res = axios.get(API_URL + "getSearchValue/" + location);
      return res;
    } catch (error) {
      return error;
    }
  }
  async getSearchCategory(rdata) {
    let levelID = rdata;
    try {
      const res = axios.get(API_URL + "getSearchCategory/" + levelID);
      return res;
    } catch (error) {
      return error;
    }
  }
  getSearchSubject(rdata) {
    let categoryID = rdata;
    return axios.get(API_URL + "getSearchSubject/" + categoryID).then(
      res => {
        return res;
      },
      error => {
        return error;
      }
    );
  }
  async getSubjects(rdata) {
    let location = rdata;
    try {
      const res = axios.get(API_URL + "getSubjects/" + location);
      return res;
    } catch (error) {
      return error;
    }
  }
  async getSearchTrakItems(rdata) {
    try {
      const res = await axios.post(
        API_URL + "getSearchTrakItems",
        {
          id: rdata.id,
          user: rdata.user
        },
        { headers: authHeader() }
      );
      //console.log(res.data.subjectInformation.level);
      return res;
    } catch (error) {
      return error;
    }
  }

  async updateQuizItem(rdata) {
    try {
      const res = await axios.post(
        API_URL + "updateQuizItem",
        {
          id: rdata.id,
          user: rdata.user
        },
        { headers: authHeader() }
      );
      return res;
    } catch (error) {
      return error;
    }
  }

  getTestItem(rdata) {
    return axios
      .get(API_URL + "getTestItem/" + rdata, { headers: authHeader() })
      .then(
        res => {
          return res;
        },
        error => {
          return error;
        }
      );
  }

  saveTestResult(rdata) {
    return axios
      .post(
        API_URL + "saveTestResult",
        {
          userID: rdata.userID,
          quizID: rdata.quizID,
          totalMark: rdata.totalMark,
          guessResult: rdata.totalPerfectResult
        },
        { headers: authHeader() }
      )
      .then(
        res => {
          return res;
        },
        error => {
          return error;
        }
      );
  }

  async getAllSubjectItems(rdata) {
    try {
      const res = axios.get(API_URL + "getAllSubjectItems/" + rdata, {
        headers: authHeader()
      });
      return res;
    } catch (error) {
      return error;
    }
  }

  async searchSubject(rdata) {
    try {
      const res = await axios.post(
        API_URL + "searchSubject",
        {
          search: rdata.searchSubject,
          locale: rdata.locale
        },
        { headers: authHeader() }
      );
      return res;
    } catch (error) {
      return error;
    }
  }
}
export default new ClientService();
